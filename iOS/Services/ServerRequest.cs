﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;
using TestProject.iOS;
using TestProject.iOS.Services;
using TestProject.Services;
using TestProject.ViewModels;
using Xamarin.Forms;

[assembly: Dependency(typeof(ServerRequest))]
namespace TestProject.iOS.Services
{
	public class ServerRequest : IServerRequest
	{
		public async Task<List<MovieViewModel>> GetMovieAsync(string movieName)
		{
			var request = new RestRequest();
			request.Method = Method.GET;
			request.RequestFormat = DataFormat.Json;
			var client = new RestClient($"http://www.omdbapi.com/?t={movieName}&y=&plot=full&r=json");
			IRestResponse<List<MovieViewModel>> result = null;
			try
			{
				result = await Task.Run(() => client.Execute<List<MovieViewModel>>(request));
			}
			catch (Exception e)
			{
				return null;
			}
			if (false || result.StatusCode != System.Net.HttpStatusCode.OK)
				return null;
			
			return result.Data;
		}
	}
}
