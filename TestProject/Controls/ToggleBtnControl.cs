﻿using System;
using Xamarin.Forms;
namespace TestProject.Controls
{
	public class ToggleBtnControl : ContentView
	{
		public ToggleBtnControl()
		{
			var stack = new StackLayout() { 
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Orientation = StackOrientation.Vertical,
				Spacing = 10
			};

			var btn = new Button()
			{
				Text = TestProject.Resources.Strings.Strings.Button,
				HorizontalOptions = LayoutOptions.Center,
				TextColor = Color.White,
				BackgroundColor = Color.FromHex("04356C"),
				IsEnabled = false,
				WidthRequest = 100,
				MinimumWidthRequest = 100,
				HeightRequest = 50,
				MinimumHeightRequest = 50
			};

			var switcher = new Switch() { 
				HorizontalOptions = LayoutOptions.Center,
				WidthRequest = 100,
				MinimumWidthRequest = 100,
				HeightRequest = 50,
				MinimumHeightRequest = 50
			};

			switcher.Toggled += (sender, e) => btn.IsEnabled = !btn.IsEnabled;

			stack.Children.Add(btn);
			stack.Children.Add(switcher);

			this.Content = stack;
		}
	}
}
