﻿using System;
using Xamarin.Forms;
using Acr.UserDialogs;
using TestProject.ViewModels;

namespace TestProject.Controls
{
	public class CellControl : ViewCell
	{
		private StackLayout _layout { get; set; }

		public static readonly BindableProperty MovieProperty =
			BindableProperty.Create(nameof(Movie), typeof(MovieViewModel), typeof(CellControl), default(MovieViewModel));

		public MovieViewModel Movie
		{
			get { return (MovieViewModel)GetValue(MovieProperty); }
			set { SetValue(MovieProperty, value); }
		}

		public CellControl()
		{
			var mainLayout = new StackLayout(){
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 5,
			};

			_layout = new StackLayout() { 
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Horizontal,
				Spacing = 10
			};

			mainLayout.Children.Add(_layout);
			this.View = mainLayout;
		}

		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if (propertyName == MovieProperty.PropertyName && Movie != null) {
				UpdateLayout();
			}
		}

		private void UpdateLayout() { 
			
			_layout.Children.Clear();
			if (!Movie.IsNeedRate)
			{
				_layout.Orientation = StackOrientation.Horizontal;
				if (Movie.Poster != "N/A")
				{
					var image = new Image()
					{
						Source = new Uri(Movie.Poster),
						WidthRequest = 50,
						MinimumWidthRequest = 50,
						HeightRequest = 50,
						MinimumHeightRequest = 50,
					};
					_layout.Children.Add(image);
				}
				var text = new Label()
				{
					Text = Movie.Title + ", " + Movie.Year
				};
				_layout.Children.Add(text);
			}
			else { 
				_layout.Orientation = StackOrientation.Vertical;
				if (Movie.Poster != "N/A")
				{
					var image = new Image()
					{
						Source = new Uri(Movie.Poster),
						WidthRequest = 50,
						MinimumWidthRequest = 50,
						HeightRequest = 50,
						MinimumHeightRequest = 50,
						HorizontalOptions = LayoutOptions.CenterAndExpand
					};
					_layout.Children.Add(image);
				}
				var rateBtn = new Button()
				{
					Text = Resources.Strings.Strings.Rate,
					TextColor = Color.Black,
					BackgroundColor = Color.FromHex("c4e6fc"),
					VerticalOptions = LayoutOptions.Center,
					HorizontalOptions = LayoutOptions.Center,
					WidthRequest = 100,
					MinimumWidthRequest = 100

				};
				rateBtn.Clicked += (sender, e) => { 
					UserDialogs.Instance.Alert(Resources.Strings.Strings.MovieRated);
				};
				_layout.Children.Add(rateBtn);
			}

			Movie.IsNeedRate = !Movie.IsNeedRate;
		}
	}
}
