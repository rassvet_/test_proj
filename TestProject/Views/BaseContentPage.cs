﻿using System;
using Xamarin.Forms;

namespace TestProject.Views
{
	public class BaseContentPage : ContentPage
	{
		public BaseContentPage()
		{
			NavigationPage.SetHasNavigationBar(this, false);
		}

		public void ShowNavBar()
		{ 
			NavigationPage.SetHasNavigationBar(this, true);
		}
	}
}
