﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using TestProject.Services;
using TestProject.Controls;
using Acr.UserDialogs;
using TestProject.ViewModels;

namespace TestProject.Views
{
	public class MovieView : ContentPage
	{ 
		private ObservableCollection<MovieViewModel> MovieList { get; set; } = new ObservableCollection<MovieViewModel>();
		private IServerRequest _server { get; set; }
		private Entry _search { get; set; }
		private IUserDialogs _dialogs { get; set; }

		public MovieView()
		{
			this.Title = TestProject.Resources.Strings.Strings.SearchMovie;
			_server = DependencyService.Get<IServerRequest>();
			var stack = new StackLayout()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 10,
				Padding = new Thickness(25, 50, 25, 50)
			};

			_search = new Entry() { 
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Placeholder = TestProject.Resources.Strings.Strings.MovieName
			};

			var listView = new ListView() { 
				HasUnevenRows = true,
				SeparatorVisibility = SeparatorVisibility.None
			};

			listView.ItemTemplate = new DataTemplate(() => {
				var view = new CellControl();
				view.SetBinding(CellControl.MovieProperty, ".");
				return view;
			});
			listView.ItemsSource = MovieList;

			var btnSearch = new Button() { 
				Text = TestProject.Resources.Strings.Strings.Search,
				WidthRequest = 100,
				MinimumWidthRequest = 100,
				HeightRequest = 30,
				MinimumHeightRequest = 30,
				BackgroundColor = Color.FromHex("0db760"),
			};

			btnSearch.Clicked += BtnSearch_Clicked;

			var searchStack = new StackLayout() { 
				Orientation = StackOrientation.Horizontal,
			};
			searchStack.Children.Add(_search);
			searchStack.Children.Add(btnSearch);
			stack.Children.Add(searchStack);
			stack.Children.Add(listView);

			this.Content = stack;
		}

		private async void BtnSearch_Clicked(object sender, EventArgs e)
		{
			var movieName = _search.Text;
			if (!string.IsNullOrEmpty(movieName))
			{
				var list = await _server.GetMovieAsync(movieName);
				if (list == null)
					return;
				MovieList.Clear();
				foreach (var movie in list)
				{
					if (movie.Poster == null || movie.Title == null || movie.Year == null)
						continue;
					MovieList.Add(movie);
					MovieList.Add(movie);
				}
			}
		}
	}
}
