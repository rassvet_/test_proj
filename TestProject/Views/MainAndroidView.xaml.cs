﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using TestProject.Views;

namespace TestProject.Views
{
	public partial class MainAndroidView : MasterDetailPage
	{
		public MainAndroidView()
		{
			InitializeComponent();
			this.Detail = new NavigationPage(new CarouselView());
		}

		public void Carousel(object sender, EventArgs e)
		{
			this.Detail = new NavigationPage(new CarouselView());
			this.IsPresented = false;
		}

		public void SearchMovie(object sender, EventArgs e)
		{
			this.Detail = new NavigationPage(new MovieView());
			this.IsPresented = false;
		}
	}
}
