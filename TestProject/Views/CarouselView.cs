﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using TestProject.Controls;
using Xamarin.Forms;

namespace TestProject.Views
{
	public class CarouselView : Xamarin.Forms.CarouselPage
	{
		public static readonly BindableProperty LogoutProperty =
			BindableProperty.Create(nameof(Logout), typeof(ICommand), typeof(CarouselView), default(ICommand));

		public ICommand Logout
		{
			get { return (ICommand)GetValue(LogoutProperty); }
			set { SetValue(LogoutProperty, value); }
		}

		private Button _logoutBtn;

		public CarouselView()
		{
			this.Title = TestProject.Resources.Strings.Strings.Carousel;
			var stack = new StackLayout();
			var img = new Image
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Source = new Uri("https://storage.googleapis.com/appconfig-media/appconfig-content/uploads/2016/04/xamarin-app-logo2.png")
			};
			_logoutBtn = new Button()
			{
				Text = TestProject.Resources.Strings.Strings.Logout,
				TextColor = Color.White,
				BackgroundColor = Color.FromHex("26a7e0")
			};

			stack.Children.Add(img);
			if(Device.OS == TargetPlatform.iOS)
				stack.Children.Add(_logoutBtn);

			var ImagePage = new ContentPage() { 
				Content = stack
			};
			var ToggleBtnPage = new ContentPage()
			{
				Content = new ToggleBtnControl()
			};
			this.Children.Add(ImagePage);
			this.Children.Add(ToggleBtnPage);
		}

		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if(propertyName == LogoutProperty.PropertyName)
				_logoutBtn.Clicked += (sender, e) => Logout.Execute(null);
		}
	}
}
