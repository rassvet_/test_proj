﻿using Microsoft.Practices.Unity;
using Prism.Navigation;
using Prism.Unity;
using TestProject.Views;
using Xamarin.Forms;
using TestProject.Services;
using System.Threading.Tasks;
using Acr.UserDialogs;

namespace TestProject
{
	public partial class App : PrismApplication
	{
		private static IUnityContainer _container;
		public static T Resolve<T>()
		{
			return _container.Resolve<T>();
		}

		public App()
		{
			InitializeComponent();
			MainPage = new MainNavigationView();
			NavigationService.NavigateAsync("Start/", animated: false);
		}

		#region overrides

		protected override void OnInitialized()
		{
		}

		protected override void RegisterTypes()
		{
			Container.RegisterInstance<INavigationService>(Container.Resolve<Prism.Unity.Navigation.UnityPageNavigationService>());
			Container.RegisterTypeForNavigation<StartView>("Start");
			Container.RegisterTypeForNavigation<LoginView>("Login");
			Container.RegisterTypeForNavigation<CreateAccountView>("CreateAccount");
			Container.RegisterTypeForNavigation<MainIosView>("MainIOS");
			Container.RegisterTypeForNavigation<MainAndroidView>("MainAndroid");
			Container.RegisterTypeForNavigation<Views.CarouselView>("Carousel");
			Container.RegisterTypeForNavigation<MovieView>("Movie");
			_container = Container;
		}

		#endregion
	}
}
