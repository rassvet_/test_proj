﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using TestProject.ViewModels;
using Xamarin.Forms;
using TestProject.Views;
using Prism.Navigation;
using TestProject.Services;

namespace TestProject.ViewModels
{
	public class MainAndroidViewModel : BaseViewModel
	{
		private INavigationService _navigationService;
		private IStorage _storage;

		private ICommand _LogoutCommand;
		public ICommand LogoutCommand
		{
			get
			{
				return _LogoutCommand ?? (_LogoutCommand = DelegateCommand.FromAsyncHandler(OnLogoutCommand));
			}
		}

		public MainAndroidViewModel(INavigationService navigationService, IStorage storage)
		{
			_navigationService = navigationService;
			_storage = storage;
		}

		private async Task OnLogoutCommand()
		{
			var user = await _storage.GetDefaultUserAsync();
			if (user != null)
			{
				user.IsLoggedIn = false;
				await _storage.SaveUserAsync(user);
			}
			await _navigationService.NavigateAsync(new Uri("/Login"));
		}
	}
}

