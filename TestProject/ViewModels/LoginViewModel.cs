﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Navigation;
using Xamarin.Forms;
using TestProject.Services;

namespace TestProject.ViewModels
{
	public class LoginViewModel : BaseViewModel
	{
		private INavigationService _navigationService;
		private IStorage _storage;

		private string _LoginText;
		public string LoginText
		{
			get { return _LoginText; }
			set { SetProperty(ref _LoginText, value); }
		}

		private string _PasswordText;
		public string PasswordText
		{
			get { return _PasswordText; }
			set { SetProperty(ref _PasswordText, value); }
		}

		private ICommand _LoginCommand;
		public ICommand LoginCommand {
			get {
				return _LoginCommand ?? (_LoginCommand = DelegateCommand.FromAsyncHandler (OnLoginCommand));
			}
		}

		private ICommand _CreateAccountCommand;
		public ICommand CreateAccountCommand {
			get {
				return _CreateAccountCommand ?? (_CreateAccountCommand = DelegateCommand.FromAsyncHandler (OnCreateAccountCommand));
			}
		}

		public LoginViewModel(INavigationService navigationService, IStorage storage)
		{
			_navigationService = navigationService;
			_storage = storage;
		}

		private async Task OnLoginCommand ()
		{
			if (string.IsNullOrEmpty (LoginText) || string.IsNullOrEmpty (PasswordText))
				await Task.FromResult<object> (null);
			var user = await _storage.LoadUserAsync(LoginText.ToLower(), PasswordText.ToLower());
			if (user != null)
			{
				if (!user.IsLoggedIn)
				{
					user.IsLoggedIn = true;
					await _storage.SaveUserAsync(user);
				}
				var view = Device.OS == TargetPlatform.iOS ? "MainIOS" : "MainAndroid";
				await _navigationService.NavigateAsync(new Uri($"/{view}"));
			}
		}

		private Task OnCreateAccountCommand ()
		{
			return _navigationService.NavigateAsync ("CreateAccount");
		}
	}
}
