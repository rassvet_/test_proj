﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Navigation;
using TestProject.ViewModels;
using TestProject.Services;
using Xamarin.Forms;
using TestProject.Models;

namespace TestProject.ViewModels
{
	public class CreateAccountViewModel : BaseViewModel
	{
		private INavigationService _navigationService;
		private IStorage _storage;

		private string _LoginText;
		public string LoginText
		{
			get { return _LoginText; }
			set { SetProperty(ref _LoginText, value); }
		}

		private string _PasswordText;
		public string PasswordText
		{
			get { return _PasswordText; }
			set { SetProperty(ref _PasswordText, value); }
		}

		private string _ConfirmPassword;
		public string ConfirmPassword
		{
			get { return _ConfirmPassword; }
			set { SetProperty(ref _ConfirmPassword, value); }
		}

		private ICommand _CreateAccountCommand;
		public ICommand CreateAccountCommand
		{
			get
			{
				return _CreateAccountCommand ?? (_CreateAccountCommand = DelegateCommand.FromAsyncHandler(OnCreateAccountCommand));
			}
		}

		public CreateAccountViewModel (INavigationService navigationService, IStorage storage)
		{
			_navigationService = navigationService;
			_storage = storage;
		}

		private async Task OnCreateAccountCommand()
		{
			if (string.IsNullOrEmpty(LoginText) || string.IsNullOrEmpty(PasswordText) || string.IsNullOrEmpty(ConfirmPassword) || PasswordText != ConfirmPassword)
				await Task.FromResult<object>(null);
			var user = new UserModel()
			{
				UserName = LoginText.ToLower(),
				UserPassword = PasswordText.ToLower(),
				IsLoggedIn = true
			};
			await _storage.SaveUserAsync(user);
			var view = Device.OS == TargetPlatform.iOS ? "MainIOS" : "MainAndroid";
			await _navigationService.NavigateAsync(new Uri($"/{view}"));
		}
	}
}
