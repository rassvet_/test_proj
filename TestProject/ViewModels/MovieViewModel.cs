﻿using System;
using TestProject.ViewModels;

namespace TestProject.ViewModels
{
	public class MovieViewModel : BaseViewModel
	{
		private string _Poster;
		public string Poster
		{
			get
			{
				return _Poster;
			}
			set
			{
				_Poster = value;
				OnPropertyChanged();
			}
		}

		private string _Title;
		public string Title
		{
			get
			{
				return _Title;
			}
			set
			{
				_Title = value;
				OnPropertyChanged();
			}
		}

		private string _Year;
		public string Year
		{
			get
			{
				return _Year;
			}
			set
			{
				_Year = value;
				OnPropertyChanged();
			}
		}

		private bool _IsNeedRate;
		public bool IsNeedRate
		{
			get
			{
				return _IsNeedRate;
			}
			set
			{
				_IsNeedRate = value;
				OnPropertyChanged();
			}
		}
	}
}
