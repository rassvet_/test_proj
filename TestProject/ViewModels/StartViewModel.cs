﻿using System;
using TestProject.Views;
using Xamarin.Forms;
using TestProject.Services;
using Prism.Navigation;
namespace TestProject.ViewModels
{
	public class StartViewModel : BaseContentPage
	{
		private INavigationService _navigationService;
		private IStorage _storage;

		public StartViewModel(INavigationService navigationService, IStorage storage)
		{
			_navigationService = navigationService;
			_storage = storage;
			TryLoginUser();
		}

		private async void TryLoginUser()
		{
			//TODO: uncomment if need to clear storage
			//await _storage.ClearStorageAsync();
			var user = await _storage.GetDefaultUserAsync();
			if (user != null && user.IsLoggedIn)
			{
				var view = Device.OS == TargetPlatform.iOS ? "MainIOS" : "MainAndroid";
				await _navigationService.NavigateAsync(new Uri($"/{view}"));
			}
			else
				await _navigationService.NavigateAsync(new Uri("/Login"));
		}
	}
}
