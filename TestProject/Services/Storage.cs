﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PCLStorage;
using TestProject.Models;
using TestProject.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(Storage))]
namespace TestProject.Services
{
	public class Storage : IStorage
	{

		private string DataPath { get; set; } = "UserData";

		public async Task SaveUserAsync(UserModel user)
		{
			try
			{
				var fileSystem = FileSystem.Current;
				var rootFolder = fileSystem.LocalStorage;
				var file = await rootFolder.CreateFileAsync(DataPath, CreationCollisionOption.ReplaceExisting);
				var serFile = JsonConvert.SerializeObject(user);
				await file.WriteAllTextAsync(serFile);
			}
			catch (Exception ex)
			{
				
			}
		}

		public async Task<UserModel> LoadUserAsync(string userName, string password)
		{
			try
			{
				var fileSystem = FileSystem.Current;
				var rootFolder = fileSystem.LocalStorage;
				var data = await rootFolder.GetFileAsync(DataPath);
				var str = await data.ReadAllTextAsync();
				var user = JsonConvert.DeserializeObject<UserModel>(str);
				if (user.UserName != userName || user.UserPassword != password)
					return null;
				return user;
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		public async Task<UserModel> GetDefaultUserAsync()
		{
			try
			{
				var fileSystem = FileSystem.Current;
				var rootFolder = fileSystem.LocalStorage;
				var data = await rootFolder.GetFileAsync(DataPath);
				var str = await data.ReadAllTextAsync();
				var user = JsonConvert.DeserializeObject<UserModel>(str);
				return user;
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		public async Task ClearStorageAsync()
		{
			try
			{
				var fileSystem = FileSystem.Current;
				var rootFolder = fileSystem.LocalStorage;
				var file = await rootFolder.GetFileAsync(DataPath);
				await file.DeleteAsync();
			}
			catch (Exception ex)
			{

			}
		}
	}
}
