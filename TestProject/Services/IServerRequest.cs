﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.ViewModels;

namespace TestProject.Services
{
	public interface IServerRequest
	{
		Task<List<MovieViewModel>> GetMovieAsync(string movieName);
	}
}
