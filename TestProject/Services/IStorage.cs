﻿using System;
using System.Threading.Tasks;
using TestProject.Models;
namespace TestProject.Services
{
	public interface IStorage
	{
		Task SaveUserAsync(UserModel user);
		Task<UserModel> LoadUserAsync(string userName, string password);
		Task<UserModel> GetDefaultUserAsync();
		Task ClearStorageAsync();
	}
}
