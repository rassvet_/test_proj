﻿using System;
namespace TestProject.Models
{
	public class UserModel
	{
		public string UserName { get; set; }
		public string UserPassword { get; set; }
		public bool IsLoggedIn { get; set; }
	}
}
